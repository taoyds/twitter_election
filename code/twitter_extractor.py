#!/usr/bin/python
# -*- coding: utf-8 -*-
'''
This module takes a dir of twitter streeming files, processes and extracts certain
features from each tweet. It saves processed tweets in dataframe format in a pickle file.
'''
__author__ = "Tao Yu"
__version__ = "Jan 09 2017"

import os
import json
import pickle
import argparse
import pandas as pd
from os import listdir
from os.path import isfile, isdir, split, exists
from itertools import groupby
from pytz import timezone
from datetime import datetime
from collections import Counter

state_dict = {
        'AK': 'Alaska',
        'AL': 'Alabama',
        'AR': 'Arkansas',
        'AS': 'American Samoa',
        'AZ': 'Arizona',
        'CA': 'California',
        'CO': 'Colorado',
        'CT': 'Connecticut',
        'DC': 'District of Columbia',
        'DE': 'Delaware',
        'FL': 'Florida',
        'GA': 'Georgia',
        'GU': 'Guam',
        'HI': 'Hawaii',
        'IA': 'Iowa',
        'ID': 'Idaho',
        'IL': 'Illinois',
        'IN': 'Indiana',
        'KS': 'Kansas',
        'KY': 'Kentucky',
        'LA': 'Louisiana',
        'MA': 'Massachusetts',
        'MD': 'Maryland',
        'ME': 'Maine',
        'MI': 'Michigan',
        'MN': 'Minnesota',
        'MO': 'Missouri',
        'MP': 'Northern Mariana Islands',
        'MS': 'Mississippi',
        'MT': 'Montana',
        'NA': 'National',
        'NC': 'North Carolina',
        'ND': 'North Dakota',
        'NE': 'Nebraska',
        'NH': 'New Hampshire',
        'NJ': 'New Jersey',
        'NM': 'New Mexico',
        'NV': 'Nevada',
        'NY': 'New York',
        'OH': 'Ohio',
        'OK': 'Oklahoma',
        'OR': 'Oregon',
        'PA': 'Pennsylvania',
        'PR': 'Puerto Rico',
        'RI': 'Rhode Island',
        'SC': 'South Carolina',
        'SD': 'South Dakota',
        'TN': 'Tennessee',
        'TX': 'Texas',
        'UT': 'Utah',
        'VA': 'Virginia',
        'VI': 'Virgin Islands',
        'VT': 'Vermont',
        'WA': 'Washington',
        'WI': 'Wisconsin',
        'WV': 'West Virginia',
        'WY': 'Wyoming'
}
state_dict_v = {y : x for x, y in state_dict.iteritems()}


def usage():
    print 'Usage: python twitter_extractor.py --rd [a dir where all tweet streem raw files located at]'


def to_est_time(tweet_time_string):
    """Convert rfc 5322 -like time string into a local time
       string in rfc 3339 -like format.

    """
    eastern = timezone('US/Eastern')
    utc = timezone('UTC')
    created_at = datetime.strptime(tweet_time_string, '%a %b %d %H:%M:%S +0000 %Y')
    utc_created_at = utc.localize(created_at)
    est_created_at = utc_created_at.astimezone(eastern)

    return est_created_at


def tweet_extract(whole_tweet):
    '''
    Given a raw tweet, extract attribute subset
    '''
    #tweet info
    created_at = whole_tweet['created_at']
    est_created_at = to_est_time(created_at)
    tweet_id = whole_tweet['id']
    text = whole_tweet['text']
    coordinates = whole_tweet['coordinates']
    favorite_count = whole_tweet['favorite_count']
    retweet_count = whole_tweet['retweet_count']
    #user info
    user = whole_tweet['user']
    user_location = user['location']
    user_id = user['id']
    #state and city info extracted from user_location
    #IF ONLY IF user_location is strictly in the format:'Herndon,VA'
    state_user_loc = None
    city_user_loc = None
    if user_location != None:
        sp = [i.strip() for i in user_location.split(',')]
        if len(sp) == 2 and sp[1].isupper() and sp[1][0].isupper():
            if sp[1] in state_dict.keys():
                state_user_loc = sp[1]
                city_user_loc = sp[0]

    #place info
    place = whole_tweet['place']
    place_full_name = None
    place_type = None
    place_name = None
    city_place = None
    state_place = None
    place_country_code = None
    place_coordinates =  None
    if place != None:
        place_full_name = place['full_name']
        place_type = place['place_type']
        place_name = place['name']
        place_country_code = place['country_code']
        bounding_box = place['bounding_box']
        if bounding_box != None:
            place_coordinates = bounding_box['coordinates']
        #state and city info extracted from place
        if place_country_code == 'US':
            if place_type == 'city' and place_full_name != None:
                _, state_place = [i.strip() for i in place_full_name.split(',')]
                city_place = place_name
            elif place_type == 'admin' and place_name in state_dict_v.keys():
                    state_place = state_dict_v[place_name]

    #mention type
    trump_only = 0
    hillary_only = 0
    both = 0
    if '@HillaryClinton' in text and '@realDonaldTrump' in text:
        both = 1
    elif '@HillaryClinton' in text:
        hillary_only = 1
    elif '@realDonaldTrump' in text:
        trump_only = 1

    #state and city combined from place and user_location
    city_comb = None
    state_comb = None
    if city_place != None:
        city_comb = city_comb
    elif city_user_loc != None:
        city_comb = city_user_loc

    if state_place != None:
        state_comb = state_place
    elif state_user_loc != None:
        state_comb = state_user_loc

    tweet = {'created_at': created_at, 'est_created_at': est_created_at, 'tweet_id': tweet_id, 'text': text,\
            "trump" : trump_only, "hillary" : hillary_only, "both" : both, 'favorite_count': favorite_count,\
            'user_location': user_location, 'user_id': user_id, 'coordinates': coordinates, 'retweet_count': retweet_count,\
            'place_full_name': place_full_name, 'place_type': place_type, 'place_name': place_name,\
            'place_country_code': place_country_code, 'place_coordinates': place_coordinates,\
            'state_user_loc': state_user_loc, 'city_user_loc': city_user_loc, 'state_place': state_place,\
            'city_place': city_place, 'state_comb': state_comb, 'city_comb': city_comb}

    return tweet


def raw_twitter_process(raw_path):
    '''
    Given a raw twitter file, process tweets to return a list of tweet with certain features
    '''
    tweets_f = []
    with open(raw_path, "r") as f:
        for line in f:
            try:
                whole_tweet = json.loads(line)
                if "created_at" in whole_tweet:
                    tweet = tweet_extract(whole_tweet)
                    tweets_f.append(tweet)
                    # check retweeted_status, if yes, then add retweet info as a new tweet
                    if 'retweeted_status' in whole_tweet.keys():
                        retweeted_status = whole_tweet['retweeted_status']
                        retweet = tweet_extract(retweeted_status)
                        tweets_f.append(retweet)
            except:
                continue

    return tweets_f


def main(rd):
    """
    process all source, ere, and annotation files in the input dir, and write all new source,
    ere, and annotation files to source_new, ere_new, and annotation_new folders in this dir.
    """
    if not isdir(rd):
        usage()
    else:
        filenames = [f for f in os.listdir(rd) if f.endswith('.txt') or f.endswith('.json')]
        print 'There are total {0} files to be processed (MAKE SURE each file ends with .txt or .json in the raw data directory)'.format(len(filenames))
        total_tweets = []
        for f in filenames:
            base_name_m = f.replace('.txt','')
            base_name = base_name_m.replace('.json','')
            print "processing file is: {0} \n".format(base_name)
            #input files
            raw_path = os.path.join(rd, f)

            #if ere and best files with the same name as source file exist
            if isfile(raw_path):
                tweets_f = raw_twitter_process(raw_path)
                total_tweets.extend(tweets_f)
                #convert to dataframe
                tweets_df = pd.DataFrame(tweets_f)
#                tweets_df.drop_duplicates(['tweet_id'], inplace=True)
                # write the dataframe to pickle file
                output_fname = base_name + '.pickle'
                output_file = os.path.join(rd, output_fname)
                with open(output_file, 'wb') as of:
                    pickle.dump(tweets_df, of)

                print '-------------in file {0} ---------------'.format(base_name)
                print 'there are {0} none unique tweets.\n'.format(tweets_df.shape[0])

            else:
                print 'WARNING: could NOT find the file in {0}.\n'.format(raw_path)

        total_tweets_df = pd.DataFrame(total_tweets)
        total_tweets_df.drop_duplicates(['tweet_id'], inplace=True)

        total_tweets_output = os.path.join(rd, "total_tweets.pickle")
        with open(total_tweets_output, 'wb') as of:
            pickle.dump(total_tweets_df, of)
        print 'There are TOTAL {0} unique tweets in the input files.\n'.format(total_tweets_df.shape[0])
        print('Successfully write the total_tweets pickle file to {0}.'.format(rd))


if __name__ == "__main__":
    try:
        parser = argparse.ArgumentParser(description='Run twitter_extractor on raw twitter stream files.')
        parser.add_argument('--rd', help='path to all tweet streem raw files located at')
        args = vars(parser.parse_args())
        print args
    except:
        usage()

    main(**args)
